import React from "react";
import Enzyme, { shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import App from "./App";
// import { render } from '@testing-library/react';
Enzyme.configure({ adapter: new Adapter() });
// console.log("enzyme", Enzyme);
test("renders learn react link", () => {
  const wrapper = shallow(<App />);

  console.log("wrapper", wrapper);
  expect(wrapper).toBeTruthy();
  // const { getByText } = render(<App />);
  // const linkElement = getByText(/learn react/i);
  //   expect(linkElement).toBeInTheDocument();
});
